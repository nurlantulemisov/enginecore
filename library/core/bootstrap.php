<?php

class EngineCore
{
    protected 
            $settings,  
            $uri,       //current uri
            $app,
			$dbApp;       //current app
    
    public function __construct($settings)
    {
        $this->settings = $settings;
        $this->uri      = urldecode(preg_replace('/\?.&*/iu', '', $_SERVER['REQUEST_URI']));
        $this->app      = false;

	    $this->connect_db($settings['database']);
        $this->process_path();
        $this->proccess_controllers();
    }
    
    public function process_path()
    {
        foreach ($this->settings['apps'] as $iterableApp) {
	        require(BASE_DIR . DS . 'apps' . DS . $iterableApp . DS . 'model.php');

            $iterableUrls = require(BASE_DIR . DS . 'apps' . DS . $iterableApp . DS . 'urls.php');

            $mathes       = array();
            
            foreach ($iterableUrls as $pattern => $method) {
                
                if(preg_match($pattern, $this->uri, $mathes)) {
                    $this->app = array('app' => $iterableApp, 'controller' => array('pattern' => $pattern,
                                                                                    'method'  => $method,
                                                                                    'request' => $mathes));
                    break(2);
                }
            }
        }
        
        if ($this->app === 'false') {
            exit('App not found');
        }
    }
    
    public function proccess_controllers()
    {
        if ($this->app || is_array($this->app)) {
            require(BASE_DIR . DS . 'apps' . DS . $this->app['app'] . DS . 'controller.php');  
            
            $controllerName = $this->app['app'] . '_Controller';
            $this->appController = new $controllerName();
            $this->appController->{$this->app['controller']['method']}($this->app['controller']['request']);
        }
    }

    public function connect_db($db)
    {
		try {
			$pdo = new PDO('mysql:host=' . $db['db_host'] . ';dbname=' . $db['db_name'], $db['db_user'], $db['db_pass']);

			$this->dbApp = Engine_Model::getInstance($pdo);
//			$this->dbApp->db($pdo);

		} catch (PDOException $e) {
			die('No connect database');
        }
    }
}

