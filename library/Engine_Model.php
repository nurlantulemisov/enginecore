<?php

class Engine_Model
{
	private static $_db;

	public function db(){
		return self::$_db;
	}

	public static function getInstance($pdo)
	{
		self::$_db = $pdo;
	}

	public function test() {
		dumper($this);
	}

	public function select($query)
	{

		$select = $this->db()->query($query);

		$rows = $select->fetch();

		return $rows;

	}
}