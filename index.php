<?php

function dumper($val) {
	echo '<pre>';
	var_dump($val);
	echo '</pre>';
}

define('BASE_DIR', getcwd());
define('DS', DIRECTORY_SEPARATOR);
define('LIB', getcwd() . DS . 'library');


require './library/starter.php';


$settings = require 'settings.php';

$app = new EngineCore($settings);

//dumper($app);
